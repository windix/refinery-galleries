require 'spec_helper'

module Refinery
  module Galleries
    describe Gallery do
      describe "validations" do
        subject do
          FactoryGirl.create(:gallery)
        end

        it { should be_valid }
        its(:errors) { should be_empty }
      end
    end
  end
end
