module Refinery
  module Galleries   
    include ActiveSupport::Configurable

    config_accessor :flickr_api_key
    config_accessor :flickr_api_secret
    config_accessor :flickr_user_id
  end
end