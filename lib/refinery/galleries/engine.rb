module Refinery
  module Galleries
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::Galleries

      engine_name :refinery_galleries

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "galleries"
          plugin.hide_from_menu = true
          plugin.pathname = root
          # plugin.activity = {
          #   :class_name => :'refinery/galleries/gallery'
          # }
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::Galleries)
      end
    end
  end
end
