Refinery::Core::Engine.routes.draw do
  # Frontend routes
  get "/gallery", :to => "galleries/galleries#index"
  get "/gallery/:set_id/:set_title", :to => "galleries/galleries#show"
end
