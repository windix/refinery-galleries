# Encoding: UTF-8

Gem::Specification.new do |s|
  s.platform          = Gem::Platform::RUBY
  s.name              = 'refinerycms-galleries'
  s.author            = 'Wei Feng <windix@gmail.com>'
  s.version           = '1.0'
  s.description       = 'Ruby on Rails Galleries extension for Refinery CMS'
  s.date              = '2015-03-08'
  s.summary           = 'Galleries extension for Refinery CMS (2.1.0)'
  s.require_paths     = %w(lib)
  s.files             = Dir["{app,config,db,lib}/**/*"] + ["readme.md"]

  # Runtime dependencies
  s.add_dependency             'refinerycms-core',    '~> 2.1.5'

  # Development dependencies (usually used for testing)
  s.add_development_dependency 'refinerycms-testing', '~> 2.1.5'
end
