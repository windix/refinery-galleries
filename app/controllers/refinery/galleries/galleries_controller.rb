module Refinery
  module Galleries
    class GalleriesController < ::ApplicationController
      before_filter :init_gallery

      def index
        @sets = @gallery.sets

        #present(@page)
      end

      def show
        @set = @gallery.set_by_id(params[:set_id])

        if (@set)
          @photos = @gallery.photos(params[:set_id])
        else
          error_404
        end

        #present(@page)
      end

      protected

      def init_gallery
        @gallery = Gallery.new
      end

    end
  end
end
