module Refinery
  module Galleries
    module GalleriesHelper
      def link_to_gallery_home
        link_to "<< Back to Gallery", "/gallery"
      end

      def gallery_set_path(set)
        "/gallery/#{set[:id]}/#{set[:title].parameterize}"
      end
    end
  end
end
