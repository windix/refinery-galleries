require 'date'

module Refinery
  module Galleries
    class Gallery
      def initialize
        init_flickr

        # for dev
        @use_cache = true

        @ttl = 1.hour
      end

      def sets
        cache.fetch(cache_key_sets, :expires_in => @ttl) do
          flickr.photosets.getList(:user_id => @user_id, :primary_photo_extras => photo_extra_options).photoset.collect { |set|
            { :id => set['id'],
              :title => set['title'],
              :primary_photo_id => set['primary'],
              :primary_photo_url => set['primary_photo_extras']['url_q'],
              :num_of_photos => set['photos'].to_i,
              :last_update => Time.at(set['date_update'].to_i).to_datetime
            }
          }.keep_if { |set|
            # remove empty sets
            set[:num_of_photos] > 0 
          }
        end
      end

      def set_by_id(set_id)
        sets.select { |set| set[:id] == set_id }.first rescue nil
      end

      def photos(set_id)
        cache.fetch(cache_key_photo(set_id), :expires_in => @ttl) do
          flickr.photosets.getPhotos(:photoset_id => set_id, :extras => photo_extra_options).photo.collect { |photo|
            { :id => photo['id'],
              :title => photo['title'],
              :url => photo['url_l']
            }
          }.delete_if { |photo|
            # remove photos with blank URL (seems a flickr bug)
            photo[:url].blank? 
          }
        end
      end

      def clear_cache
        sets = cache.read(cache_key_sets)
        sets.each { |set| cache.delete(cache_key_photo(set[:id])) } if sets

        cache.delete(cache_key_sets)
      end

      private

      def init_flickr
        FlickRaw.api_key = Galleries.flickr_api_key
        FlickRaw.shared_secret = Galleries.flickr_api_secret
        @user_id = Galleries.flickr_user_id
      end

      def photo_extra_options
        # https://www.flickr.com/services/api/flickr.photos.getSizes.html
        # q - large square (150 x 150)
        # l - large (1024 x 768)
        # o - original
        "url_q,url_l,url_o"
      end

      def cache
        @use_cache ? Rails.cache : ActiveSupport::Cache.lookup_store(:null_store)
      end

      def cache_key_sets
        "flickr.sets"
      end

      def cache_key_photo(set_id)
        "flickr.set_#{set_id}.photos"
      end

    end
  end
end



